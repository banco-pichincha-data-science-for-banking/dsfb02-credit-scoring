# DSFB02 Credit Scoring

Es un proyecto completo en el que se muestra el proceso de construcción de un modelo de Credit Scoring para Riesgos 

Nota:

> Todo archivo contenido en los presentes repositorios es considerado por los autores como privado. Por lo que, los usuarios que tengan acceso se obligan a usar exclusivamente durante la duración del proyecto Data Science for Banking. Además de proteger y no revelar, divulgar, exhibir, mostrar y/o comunicar la información contenida en estos repositorios.

Contenido

* En la carpeta [code](./code) se encuentran los scripts de Python y Jupyter Notebook.
* En [document](./document) se almacenan los documentos relacionados a la elaboración del proyecto. Debera de incluir los  temas elegidos de acuerdo a la metodología de proyectos de ciencia de datos elegidos (CRISP-DM, Microsoft, entre otros.).
* En result se guardan todos los resultados obtenidos en el desarrollo del proyecto.
* Por último, en src se almacena todos las fuentes utilizadas en el proyecto, incluye archivos multimedia (imagen, videos, audio), base de datos, tablas, fuentes de información, entre otros

![Ander Vargas](https://media.licdn.com/dms/image/C4E03AQEIVnrRmdtfdA/profile-displayphoto-shrink_200_200/0?e=1568246400&v=beta&t=lhmL6fugdHmzj7hq-rNzeyjv7cyY1jPNFbA5YoBfW2w)

