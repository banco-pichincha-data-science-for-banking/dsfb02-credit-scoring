#Data Analysis - Session 1
#1. Datos Num�ricos
#Asignaci�n de un valor a una variable
z=5
z
x<-8
x
rm(z)
z
#Asignaci�n de n�meros enteros
y<-45L
y
class(y)

y<-as.integer(4/2); y
class(y)

#Asignaci�n de n�meros complejos

n<-15+3i
n
class(n)

m<--1+0i
m
v<-sqrt(m)
v
#valores num�ricos especiales

f<-1/0
f

h<-0/0
h

#Vectores

w<-vector("numeric",10)
w
x<-2
x

v<-c(4,6,7)
c(4,6,7)-> m

x<-c("Cesar","Carmen")
x

u<-vector("logical",6)
u

#Secuencias

n<-seq(from=2,to=10,by=3)
n

m=seq(from=4,by=2,length.out=8)
m

u<-c(2,5,-2)
v<-rep(u,times=10)
v

x<-c(2,7,6)
y<-c(9,0,8)
z<-c(x,y)
z
z[4]
z[3]+z[6]
z[4]<-z[1]+z[6]
z

#Asignaci�n de nombres a los elementos
caramelos<-c(3,9,5,6,3)
names(caramelos)<-c("lim�n","perita","monterrico","frutas","menta")
caramelos
caramelos<-c(limon=3,perita=9,monterrico=5,frutas=6,menta=3)
caramelos
#Operaciones
caramelos[c("perita","frutas")]
caramelos["monterrico"]<-12
caramelos

v<-c(4,5)-c(8,9)
v
w<-c(3,4,2)^(3:1)
w
#Longitud de vectores
u<-3:45
v<-c(4,5,6)
w<-c(u,v)
w

length(w)

#Matrices
m<-matrix(11:46,nrow=6,ncol=4,byrow=FALSE)
m
class(m)
m[2,3]
#Fila 5
m[5,]
#Columna 3
m[,3]

#Asignaci�n de nombres
rownames(m)<-c("UNO","DOS","TRES","CUATRO","CINCO","SEIS")
colnames(m)<-c("uno","dos","tres","cuatro")
m
#Producto de matrices
m1<-rbind(c(3,4,5,6),c(2,4,6,1))
m1
m2<-cbind(c(1.5,6,3,4),c(2.8,4,5.8))
m2
m1%*%m2
#transpuesta de una matriz
t(m2)

#Factores
sexo<-c("Hombre", "Mujer", "Mujer", "Hombre", "Hombre" ,"Mujer", "Hombre")
nombre<-c("Pedro", "Alicia", "Ana", "Manolo", "Germ�n", "Mar�a", "Juan")
sexo<-factor(sexo)
sexo

Personas<-c("Irina","Vilma","Jose","pierre","Ivonne")
mes.nacimiento<-c("Enero","Junio","Marzo","Noviembre","Noviembre")
mes.nacimientof<-factor(mes.nacimiento)
mes.nacimientof


#Listas
familia<-list("Maria","Juan",20,c("Jes�s","Yoko"),c(15,2))
familia

familia<-list(madre="Maria",padre="Juan",anos_cas=20,hijos=c("Jes�s","Yoko"),edades=c(15,2))
familia

#Accediendo a los elementos de la lista
familia$hijos
#Modificar elementos
familia[["padre"]]<-"Luis"
familia

#data frames
A <- cbind(ord=1:3, edad=c(30L, 26L, 9L)) 
A
B<- c(1.80, 1.72, 1.05)
B
C <- data.frame(familia=c("Padre", "Madre", "Hijo"),A, estatura=B)
C
C$estatura

C[2]
C[2,2]
C[,3]<-C[,3]-2
C
#Funciones
hipotenusa <- function(x, y) {
  r<-sqrt(x^2 + y^2)
  print(r)
}
hipotenusa

hipotenusa <- function(x=3, y=4) {
  r<-sqrt(x^2 + y^2)
  print(r)
}

hipotenusa(x=sqrt(3),y=1)

#If
x <- 20
if (x > 5) print("Es mayor")

#If - Else

if (10 > x) { # 1�. bloque
  print("RANGO MENOR")
} else if ( 10 <= x && x <= 20) { # 2�. bloque
  print("primer renglon"); print("RANGO MEDIO")
} else { # 3�. bloque
  print("RANGO MAYOR")
}

#Recursividad
letras <- c("A", "n", "i", "i", "t", "a")
for (i in 1:6) {
  print(letras[i])
}

#Repetici�n con condici�n
i <- 1
while (i <= 6) {
  print(letras[i])
  i <- i + 1
}

#Repetici�n infinita
i <- 1
repeat {
  print(letras[i])
  i <- i + 1
  if (i > 6)
    break  #Condici�n que interrumpe el ciclo
}



### Libro
# https://es.r4ds.hadley.nz/r-markdown.html


